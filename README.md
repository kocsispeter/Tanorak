ÉVFOLYAM ZH

Feladat

Egy iskolában sokféle órát tartanak. Ismerjük az összes tanár nevét, és minden egyes órának néhány fontos jellemzőjét. Írjon programot az alábbi részfeladatok megoldására:

a) Mennyi az egyes tanárok összóraszáma?                    2
b) Ki a legnagyobb óraszámú tárgyat tanító tanár?           2
c) Milyen órái vannak egy adott osztály tanulóinak?         2
d) Sorolja föl az iskolában tanított tantárgyakat!          4

A standard bemenet első sorában az órák N száma ( 1≤N≤50 ) és a tanárok T száma ( 1≤T≤30 ), valamint egy OA osztályt azonosító szöveges kód van (pl. 1A , 14C ), a c) részfeladathoz. A következő T sorban a tanárok neve található (nem üres, nem ismétlődő, akár több szóból is áll-hat), ezt követően 4*N sorban az órák leírása: a tárgy neve (nem üres, akár több szóból is állhat), a tanár előző felsorolásbeli sorszáma ( 1≤Tanár i ≤T ), a tárgy óraszáma ( 1≤ÓraSzám i ≤9 ), azt az osztályt azonosító kód, amelyben tanítják. A szöveges adatok nem tartalmaznak ékezetes betűket, és következetesen nagybetűsek; így a szokásos rendezési relációk alkalmazhatók rájuk. A bemenet helyes, az ellenőrzés szükségtelen.
A standard kimenet első sorába az 

a) feladat eredményét jelentő T egész számot írja! A második sor a 

b) feladathoz tartozik, benne egy tanár neve szerepeljen! (Nem egyértelmű esetben a névsor
szerinti elsőt írja ki!) A harmadik sorba a 

c) feladat eredménye kerüljön: azaz egy felsorolás, amely
a felsorolás hosszával kezdődik (0 is lehet!), ezután a tantárgyak nevei szerepelnek! A sorban lévő értékeket egymástól egy vessző válassza el! (A tárgynevek sorrendje tetszőleges.) A negyedik sor a

d) részfeladat eredményét tartalmazza! Itt szintén egy tantárgyfelsorolás szerepeljen, az előző részfeladatban leírt formai szempontok figyelembe vételével!


A standard kimenetre tehát 4 sort kell kiírni! A részfeladatok válaszai egy-egy sorba írandók, a
feladatkitűzés sorrendjében. Ha egy részfeladathoz több eredményadat tartozik, akkor ügyel-
jen az elválasztó jelre, ami most az a) részfeladat esetében szóköz, a c) és a d) esetében
vessző. Ha a részfeladatok valamelyikét nem tudja megoldani, akkor az eredménye helyett egy
üres sort írjon ki! Ezeken kívül semmi mást nem szabad kiírni! A program végleges változatában
ne maradjon billentyűre várakozás (a tesztrendszer nem képes billentyűket nyomogatni )!
Csak a feladat érdemi megoldását célzó programokat értékelünk, a tes