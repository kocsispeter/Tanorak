#include <iostream>
#include <stdlib.h>

using namespace std;

const int MaxN=50;
const int MaxT=30;

///bemeneti paraméterek:
int N;///órák száma
int T;///tanárok száma
string OA;///osztálykód

typedef string TNevek[MaxT+1];
TNevek tNk;///tanárok nevei

typedef struct
{
    string targyNev;
    int tanKod;
    int oraDb;
    string osztKod;
} TOra;

typedef TOra TOrak[MaxN+1];
TOrak orak;///órák leírása

///kimeneti paraméterek:
typedef int TTanDb[MaxT+1];
TTanDb A_tanDb;
string B_maxTanNev;
typedef string TTargyDb[MaxN+1];
int C_tDb;
TTargyDb C_targyak;
int D_tDb;
TTargyDb D_targyak;

void hiba(string uzenet, int megallasiKod);
void beolvasas();
int main()
{
    clog << "Orak feldolgozasa" << endl;
    beolvasas();
///A) feladat:
    for (int i=1; i<=T; ++i) A_tanDb[i]=0;
    for (int i=1; i<=N; ++i)
    {
        A_tanDb[orak[i].tanKod]+=orak[i].oraDb;
    }
    for (int i=1; i<=T; ++i) cout << A_tanDb[i] << " ";
    cout << endl;

///B) feladat:
    int maxI=1;
    string B_maxTanNev=tNk[orak[1].tanKod];
    for (int i=2; i<=N; ++i)
    {
        if (orak[i].oraDb>orak[maxI].oraDb ||
                (orak[i].oraDb==orak[maxI].oraDb && B_maxTanNev>tNk[orak[i].tanKod]))
        {
            maxI=i;
            B_maxTanNev=tNk[orak[i].tanKod];
        }
    }
    cout << B_maxTanNev << endl;
///C) feladat:
    C_tDb=0;
    for (int i=1; i<=N; ++i)
    {
        if (orak[i].osztKod==OA)
        {
            C_targyak[++C_tDb]=orak[i].targyNev;
        }
    }
    cout << C_tDb;
    for (int i=1; i<=C_tDb; ++i) cout << ',' << C_targyak[i];
    cout << endl;

///D) feladat:
    D_tDb=0;
    for (int i=1; i<=N; ++i)
    {
        int j=1;
        while (j<=D_tDb && orak[i].targyNev!=D_targyak[j])
        {
            ++j;
        }
        if (j>D_tDb)
        {
            D_targyak[++D_tDb]=orak[i].targyNev;
        }
    }
    cout << D_tDb;
    for (int i=1; i<=D_tDb; ++i) cout << ',' << D_targyak[i];
    cout << endl;
    return 0;
}

void hiba(string uzenet, int megallasKod)
{
    cerr << "\n" << uzenet << " (" << megallasKod << ")\n";
    exit(megallasKod);
}
void beolvasas()
{
    clog << "Orak szama/Tanarok szama/Osztalykod:";
    cin >> N;
    if (cin.fail() || cin.peek()!=' ' || N<1 || N>MaxN) hiba("Hibas oraszam!",11);
    cin >> T;
    if (cin.fail() || cin.peek()!=' ' || T<1 || T>MaxT) hiba("Hibas tanarszam!",12);
    cin >> OA;
    if (OA.size()==0) hiba("Hibas osztalykod!",13);
    string tmp;
    getline(cin,tmp);//sorvégjel megevése 7

    for (int i=1; i<=T; ++i)
    {
        clog << i << ". tanar neve:";
        getline(cin,tNk[i]);
        if (tNk[i].size()==0) hiba("Hibas tanarnev!",20+i);
    }
    for (int i=1; i<=N; ++i)
    {
        clog << i << ". targynev:";
        getline(cin,orak[i].targyNev);
        if (orak[i].targyNev.size()==0) hiba("Hibas targynev!",MaxT+20+i);
        clog << i << ". tanarkod:";
        cin >> orak[i].tanKod;
        if (cin.fail() || cin.peek()!='\n' || orak[i].tanKod<1 || orak[i].tanKod>T) hiba("Hibas tanarkod!",MaxT+20+i);
        string tmp;
        getline(cin,tmp);//sorvégjel megevése
        clog << i << ". oraszam:";
        cin >> orak[i].oraDb;
        if (cin.fail() || cin.peek()!='\n' || orak[i].oraDb<1 || orak[i].oraDb>9) hiba("Hibas oraszam!",MaxT+20+i);
        getline(cin,tmp);//sorvégjel megevése
        clog << i << ". osztalykod:";
        getline(cin,orak[i].osztKod);
        if (orak[i].osztKod.size()==0) hiba("Hibas osztalykod!",MaxT+20+i);
    }
}
